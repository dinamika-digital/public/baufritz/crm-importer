<?php

// namespace ...;

use Google\Auth\ApplicationDefaultCredentials;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class BaufritzClient {
  public function __construct($target_audience) {
    $this->targetAudience = $target_audience;
  }

  public function send($data, $chunk_size = 1000) {
    $this->middleware = ApplicationDefaultCredentials::getIdTokenMiddleware($this->targetAudience);
    $this->stack = HandlerStack::create();
    $this->stack->push($this->middleware);

    $count  = count($data);
    $uid    = uniqid();
    $chunks = array_chunk(self::filterEmptyValues($data), $chunk_size);

    $output = [];
    foreach ($chunks as $chunk) {
      $response = $this->sendChunk($chunk, $uid, $count);
      $count -= $response['errorCount'];
      $output[] = $response;
    }

    return $output;
  }

  private function sendChunk($data, $uid, $count) {
    $body = [
      'uid'   => $uid,
      'count' => $count,
      'data'  => json_encode($data),
    ];

    $response = $this->getClient()->post('/', [
      'form_params' => $body,
    ]);

    return json_decode($response->getBody(), TRUE);
  }

  private function getClient() {
    return new Client([
      'base_uri' => $this->targetAudience,
      'handler'  => $this->stack,
      'auth'     => 'google_auth',
    ]);
  }

  private static function filterEmptyValues($array) {
    return array_filter(array_map(function ($value) {
      return is_array($value) ? self::filterEmptyValues($value) : $value;
    }, $array));
  }
}
